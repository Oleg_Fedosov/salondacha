/*init.js*/
/*
    init.js v2.0
    Wezom wTPL v4.0.0
*/
window.wHTML = (function($){

    /* Приватные переменные */

        var varSeoIframe = 'seoIframe',
            varSeoTxt = 'seoTxt',
            varSeoClone = 'cloneSeo',
            varSeoDelay = 200;

    /* Приватные функции */

        /* проверка типа данных на объект */
        var _isObject = function(data) {
            var flag = (typeof data == 'object') && (data+'' != 'null');
            return flag;
        },

        /* создание нового элемента элемента */
        _crtEl = function(tag, classes, attrs, jq) {
            var tagName = tag || 'div';
            var element = document.createElement(tagName);
            var jQueryElement = jq || true;
            // если классы объявлены - добавляем
            if (classes) {
                var tagClasses = classes.split(' ');
                for (var i = 0; i < tagClasses.length; i++) {
                    element.classList.add(tagClasses[i]);
                }
            }
            // если атрибуты объявлены - добавляем
            if (_isObject(attrs)) {
                for (var key in attrs) {
                    var val = attrs[key];
                    element[key] = val;
                }
            }
            // возвращаем созданый елемент
            if (jQueryElement) {
                return $(element);
            } else {
                return element;
            }
        },

        /* создаем iframe для сео текста */
        _seoBuild = function(wrapper) {
            var seoTimer;
            // создаем iframe, который будет следить за resize'm окна
            var iframe = _crtEl('iframe', false, {id: varSeoIframe, name: varSeoIframe});
            iframe.css({
                'position':'absolute',
                'left':'0',
                'top':'0',
                'width':'100%',
                'height':'100%',
                'z-index':'-1'
            });
            // добавляем его в родитель сео текста
            wrapper.prepend(iframe);
            // "прослушка" ресайза
            seoIframe.onresize = function() {
                clearTimeout(seoTimer);
                seoTimer = setTimeout(function() {
                    wHTML.seoSet();
                }, varSeoDelay);
            };
            // вызываем seoSet()
            wHTML.seoSet();
        };

    /* Публичные методы */

        function Methods(){}

        Methods.prototype = {

            /* установка cео текста на странице */
            seoSet: function() {
                if ($('#'+varSeoTxt).length) {
                    var seoText = $('#'+varSeoTxt);
                    var iframe = seoText.children('#'+varSeoIframe);
                    if (iframe.length) {
                        // если iframe сущствует устанавливаем на место сео текст
                        var seoClone = $('#'+varSeoClone);
                        if (seoClone.length) {
                            // клонеру задаем высоту
                            seoClone.height(seoText.outerHeight(true));
                            // тексту задаем позицию
                            seoText.css({
                                top: seoClone.offset().top
                            });
                        } else {
                            // клонера нету - бьем в колокола !!!
                            console.error('"'+varSeoClone+'" - не найден!');
                        }
                    } else {
                        // если iframe отсутствует, создаем его и устанавливаем на место сео текст
                        _seoBuild(seoText);
                    }
                }
            },

            /* magnificPopup gallery */
            mfi: function() {
                $('.mfiG').magnificPopup({
                    type: 'image',
                    gallery: {
                        enabled: true
                    },
                    closeBtnInside: true,
                    removalDelay: 300,
                    mainClass: 'zoom-in'
                });
            },

            /* magnificPopup ajax */
            mfiAjax: function() {
                $('body').magnificPopup({
                    delegate: '.mfiA',
                    callbacks: {
                        beforeOpen: function(){
                            $.magnificPopup.close();
                        },
                        elementParse: function(item) {
                            this.st.ajax.settings = {
                                url: item.el.data('url'),
                                type: 'POST',
                                data: (typeof item.el.data('param') !== 'undefined') ? item.el.data('param') : ''
                            };
                        },
                        ajaxContentAdded: function(el) {
                            wHTML.validation();
                        }
                    },
                    type: 'ajax',
                    removalDelay: 300,
                    mainClass: 'zoom-in'
                });
            },

            /* оборачивание iframe и video для адаптации */
            wTxtIFRAME: function() {
                var list = $('.wTxt').find('iframe').add($('.wTxt').find('video'));
                if (list.length) {
                    // в цикле для каждого
                    for (var i = 0; i < list.length; i++) {
                        var element = list[i];
                        var jqElement = $(element);
                        // если имеет класс ignoreHolder, пропускаем
                        if (jqElement.hasClass('ignoreHolder')) {
                            continue;
                        }
                        if (typeof jqElement.data('wraped') === 'undefined') {
                            // определяем соотношение сторон
                            var ratio = parseFloat((+element.offsetHeight / +element.offsetWidth * 100).toFixed(2));
                            if (isNaN(ratio)) {
                                // страховка 16:9
                                ratio = 56.25;
                            }
                            // назнчаем дату и обрачиваем блоком
                            jqElement.data('wraped', true).wrap('<div class="iframeHolder ratio_' + ratio.toFixed(0) + '" style="padding-top:'+ratio+'%;""></div>');
                        }
                    }
                    // фиксим сео текст
                    this.seoSet();
                }
            }
        };

    /* Объявление wHTML и базовые свойства */

    var wHTML = $.extend(true, Methods.prototype, {});

    return wHTML;

})(jQuery);



var sFlag = true;

jQuery(document).ready(function($) {


    function sliderFred(parent, width, responsive, auto, items, fx, height, direction){
        parent.find('ul').carouFredSel({
            width: width,
            responsive: responsive,
            direction: direction,
            auto: auto,
            scroll: {
                items: items,
                duration: 1000,
                timeoutDuration: 3000,
                fx: fx,
                pauseOnHover: true
            },
            items: {
                height: height
            },
            prev: parent.find('.wPrev'),
            next: parent.find('.wNext'),
            swipe: true
        }, {
            transition: transitFlag
        });
    }

    function sliderFredI(parent, width, responsive, auto, items, height, fx, direction){
        parent.find('ul').carouFredSel({
            width: width,
            responsive: responsive,
            direction: direction,
            auto: auto,
            scroll: {
                items: items,
                duration: 1000,
                timeoutDuration: 3000,
                fx: fx,
                pauseOnHover: true,
                onBefore: function(){
                    sFlag = false;
                },
                onAfter: function(){
                    sFlag = true;
                }
            },
            items: {
                height: height
            },
            swipe: true
        }, {
            transition: transitFlag
        });
    }

    function sliderFred2(parent, width, height, auto, items, fx){
        parent.find('ul').carouFredSel({
            width: width,
            height: height,
            direction: 'up',
            auto: auto,
            scroll: {
                items: items,
                duration: 1000,
                timeoutDuration: 3000,
                fx: fx,
                pauseOnHover: true
            },
            prev: $('.wPrevItem'),
            next: $('.wNextItem'),
            swipe: true
        }, {
            transition: transitFlag
        });
    }

    function sliderFredLeft(parent, width, height, auto, items, fx){
        parent.find('ul').carouFredSel({
            width: width,
            height: height,
            direction: 'left',
            auto: auto,
            scroll: {
                items: items,
                duration: 1000,
                timeoutDuration: 3000,
                fx: fx,
                pauseOnHover: true
            },
            prev: $('.wPrevItem'),
            next: $('.wNextItem'),
            swipe: true
        }, {
            transition: transitFlag
        });
    }

    // поддержка cssanimations
    transitFlag = Modernizr.cssanimations;

    // очитска localStorage
    localStorage.clear();

    // сео текст
    wHTML.seoSet();

    // magnificPopup inline
    wHTML.mfi();

    // magnificPopup ajax
    wHTML.mfiAjax();

    // валидация форм
    wHTML.validation();

    if($('.wSearchFixBtn').length) {
        $('.wSearchFixBtn').on('click', function(){
            if($('.wSearch').hasClass('wShow')) {
                $('.wSearch').removeClass('wShow');
            }
            else {
                $('.wSearch').addClass('wShow');                
            }
        });
        $('body').on('click', function(e){
            if(!$(e.target).closest('.wHeader').length) {
                $('.wSearch').removeClass('wShow');
            }
        });
    }

    $('body').on('click', '.wCloseMfi', function(){
        $.magnificPopup.close();
    });
    $('body').on('click', '.mfiCallOrder', function(){
        $.magnificPopup.close();
        setTimeout(function(){
            $('.wCallBack').trigger('click');
        }, 300);
    });

    $(window).resize(function(){
        if($(window).width() <= 1024 && $('.wSearch').length) {
            $('.wSearch').addClass('wSearchFix');
        }
        else {
            $('.wSearch').removeClass('wSearchFix');
        }
        // сео текст при ресайзе
        wHTML.seoSet();
        if($('.wItemDopSlider').length) {
            if($(window).width()<400){
                $('.wItemDopSlider ul').trigger('destroy');
                sliderFredLeft($('.wItemDopSlider'), '100%', '100%', false, 1, 'scroll', 'auto');
            }
            else {
                $('.wItemDopSlider ul').trigger('destroy');
                sliderFred2($('.wItemDopSlider'), '100%', '100%', false, 1, 'scroll', 'auto', 'up');
            }
        }
    });

    $(window).load(function() {
        // оборачивание iframe и video для адаптации
        wHTML.wTxtIFRAME();

        if($(window).width() <= 1024 && $('.wSearch').length) {
            $('.wSearch').addClass('wSearchFix');
        }
        else {
            $('.wSearch').removeClass('wSearchFix');
        }

        if($('.wMainSlider').length) {
            sliderFred($('.wMainSlider'), 'auto', true, true, 1, 'crossfade', '20.8%', 'left');
        }
        if($('.wSliderOther').length) {
            $('.wSliderOther').each(function(){
                sliderFred($(this), '100%', false, false, 1, 'scroll', 'auto', 'left');
            });
        }
        if($('.wItemDopSlider').length) {
            sliderFredI($('.wItemMainPhoto'), 'auto', true, false, 1, '71.3%', 'crossfade', 'left');
            $('.wItemDopSlider a').click(function() {
                if(sFlag) {
                    $('.wItemMainPhoto ul').trigger('slideTo', '#' + this.href.split('#').pop());
                    $('.wItemDopSlider a').removeClass('cur');
                    $(this).addClass('cur');
                    return false;
                }
                else {
                    return false;
                }
            });
            if($(window).width()<400){
                $('.wItemDopSlider ul').trigger('destroy');
                sliderFredLeft($('.wItemDopSlider'), '100%', '100%', false, 1, 'scroll', 'auto');
            }
            else {
                $('.wItemDopSlider ul').trigger('destroy');
                sliderFred2($('.wItemDopSlider'), '100%', '100%', false, 1, 'scroll', 'auto', 'up');
            }
        }

        if($('#mainMenu').length) {
            $('#mainMenu').mmenu({
                navbar: {
                    title: 'Меню'
                }
            });
        }

        if($('#catalogMenu').length) {
            $('#catalogMenu').mmenu({
                navbar: {
                    title: 'Каталог'
                }
            });
        }

        if ($('#scrollerUp').length) {
            $(window).scroll(function() {
                // показ/скрытие кнопки
                ($(this).scrollTop() > 300) ? $('#scrollerUp').stop().show(300) : $('#scrollerUp').stop().hide(300);
            });
            $('#scrollerUp').on('click', function() {
                // расчет времени скролла от высоты документа и текущей позиции - контролл для варирования скорости -> множитель 1000
                var scrollerUpSdeed = ($(document).scrollTop() / $(document).height()).toFixed(2) * 1000;
                $('body, html').stop().animate({
                    scrollTop: 0
                }, scrollerUpSdeed);
            });
        }

        if($('.wSelect2').length) {
            $('.wSelect2').select2({
                minimumResultsForSearch: -1
            })
        }

        // Стилизация карты
        if ($("#ggl_map").length) {
            var styles = [
                {
                    "stylers": [
                        { "gamma": 0.93 },
                        { "saturation": -80 },
                        { "lightness": 2 }
                    ]
                },{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        { "color": "#34be22" }
                    ]
                },{
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        { "color": "#e9e9e9" }
                    ]
                },{
                    "featureType": "landscape.natural",
                    "elementType": "geometry",
                    "stylers": [
                        { "color": "#f2f2f2" }
                    ]
                },{
                    "featureType": "poi",
                    "stylers": [
                        { "saturation": -60 }
                    ]
                }
            ]
            var mapdata = [
                parseFloat($("#ggl_map").attr('data-map-x'), 10),
                parseFloat($("#ggl_map").attr('data-map-y'), 10),
                parseInt($("#ggl_map").attr('data-map-z'), 10),
                $('#ggl_map').attr('data-map-icon')
            ];
            function loadGoogleMap() {
                var myLatlng = new google.maps.LatLng(mapdata[0], mapdata[1]);
                var myOptions = {
                    zoom: mapdata[2],
                    scrollwheel: false,
                    center: myLatlng,
                    zoomControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    scrollwheel: true,
                    panControl: false,
                    mapTypeControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: styles
                };
                var map = new google.maps.Map(document.getElementById("ggl_map"), myOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    icon: mapdata[3]
                });
            }
            setTimeout(function(){
                loadGoogleMap();
            }, 300);
        }

    });

});
//# sourceMappingURL=maps/inits.js.map
