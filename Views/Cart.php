<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<head>
	<!-- (c) студия Wezom | www.wezom.com.ua-->
    <?php echo Core\Widgets::get('Head', $_seo); ?>
</head>
<body class="indexPage">
    <div class="wWrapper">
        <?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
		<!-- .wHeader -->
        <div class="wContainer"> 
			<div class="wOrderPage">
				<div class="wSize">
					<?php echo $_breadcrumbs; ?>				
					<!-- breadcrumbs -->
					<div class="wSliderTitle">
						<span>Оформление заказа</span>
					</div>
					<?php echo $_content; ?>					
				</div>					
			</div>
			<?php echo Core\Widgets::get('Index_Advantages'); ?>
			<div class="wSeoBlock">
				<div id="cloneSeo"></div>
			</div>
			<!-- .wConteiner -->
        </div>
    </div>
    <?php echo Core\Widgets::get('Footer', array('counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config)); ?>
		<!-- .wFooter -->
	<?php echo Core\Widgets::get('HiddenData'); ?>
</body>
</html>