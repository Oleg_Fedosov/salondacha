<div class="wMiddle">
	<div class="wSliderTitle"><span>Контакты</span></div>
	<div class="wContactMain">
		<div class="wContactsInfo w_fll">
			<div class="wContactsItem">
				<div class="wContSvg wContPhone w_fll">
					<svg>
						<use xlink:href="#icon_cont1"/>
					</svg>
				</div>
				<div class="wContOpus w_ovh">
					<div class="wContInfoTitle">Контактный телефон:</div>
						<div class="wContInfoPhone">
							<?php if ($phone1 = trim(Core\Config::get('static.phone1'))): ?>
								<a href="tel:+<?php echo preg_replace('#\D#', '', $phone1); ?>?call"><?php echo $phone1; ?></a>
							<?php endif ?>
							<br>
							<?php if ($phone2 = trim(Core\Config::get('static.phone2'))): ?>
								<a href="tel:+<?php echo preg_replace('#\D#', '', $phone2); ?>?call"><?php echo $phone2; ?></a>
							<?php endif ?>
						</div>
					<div class="wContText">Если у Вас возникли вопросы, звоните нам на предоставленные номера. Звонки на территории РФ бесплатные</div>
				</div>
			</div>
			<div class="wContactsItem">
				<div class="wContSvg wContAdress w_fll">
					<svg>
						<use xlink:href="#icon_cont2"/>
					</svg>
				</div>
				<div class="wContOpus w_ovh">
					<div class="wContInfoTitle">Адрес офиса:</div>
					<div class="wContInfoAdress"><?php echo nl2br(Core\Config::get('static.office_address')); ?></div>
					<div class="wContText">Наш офис работает с 9:00 до 18:00<br>Вы можете обратиться к нам по предоставленному адресу</div>
				</div>
			</div>
		</div>
		<div class="wContactsForm w_flr">
			<div class="wContTitle">напишите нам</div>
			<div data-form="true" data-ajax="contacts"  class="wForm wFormDef">
				<div class="wFormRow">
					<label for="nameC">Ваше имя</label>
					<input type="text" required name="nameC" id="nameC" data-name="name" data-rule-word="true" data-rule-minlength="2" class="wInput">
				</div>
				<div class="wFormRow">
					<label for="phoneC">Ваш телефон</label>
					<input type="tel" required name="phoneC" id="phoneC" data-name="phone" "data-rule-phoneru="true" class="wInput">
				</div>
				<div class="wFormRow">
					<label for="textC">Ваше письмо</label>
					<textarea required name="textC" id="textC" data-name="text" data-rule-minlength="10" class="wTextarea"></textarea>
				</div>
				<div class="wFormRow">
					<button class="wBtn w_success wSubmit"><span>отправить письмо</span></button>
				</div>
				<?php if(array_key_exists('token', $_SESSION)): ?>
					<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
				<?php endif; ?>
			</div>
		</div>
		<div class="w_clear"></div>
	</div>
	<div class="wMapBlock">
		<div class="wSliderTitle"><span>Наше расположение</span></div>
		<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<div class="wMap">
			<div id="ggl_map" style="position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);" data-map-x="55.8872117" data-map-y="37.7561873" data-map-z="12" data-map-icon="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAA+CAYAAABQmvmRAAAIv0lEQVRo3sWaC1SUZRrHP8jSymypc9JWqaytPV5qz64pKoQiLsnFMcu2NPekudll2bL2aHZRC8vKPWZ1Ogpq6oqGpiCgIJipyE2RRK6JhAoOMCAiw9xnmHl6nvm+IZj5rsMMPZz/mTnfvJcfz7zv877v8w4DAIy3mniAGYaKQX2KOoiqRrWi9JzofRX32TrULNTQ/vTpDeQg1DxURujBIZbX8mbAlpoP4bg6FX7RVkKHuQ1M3QYw2vT4vhXqOsvhR/UB2FK9Bl7Nmw5Yx4x101BzUYF+A+ZAX0HVv3wyDA5f2Ql6qxaUms7aCZmXt8Pi45MB26pDLVICLhd2KqqMPHS+vQB8Zeeu5cGSE1MJvBQ1od/A2MhNqDUzM++2HWnYDf4wB/5lNyTDjIw/WLGvd1ABXgFjxVtpsiw9GQ6txquKIDZVveeUEmsxNMBLJ6aQt/ejhigCxgq3oQo/OLMALHazoo5T6xOpU6fSLiUpqmvpNsE7xc9Q3aOoW2QBU0FU7pqSfzq/LiVW3l7YA+sSPVNidkc3vHv6H1Q3nYakHOCNbxZEOyu6m8GhhS5Hu6B3Yg8HewDHZQWD1W5RBE3lX82LoPr/EwXGAnPmHBkNXZYO3oYOWD+HhcYREG/6CySYVZBu2wgX7Wedn+2sXQd/24WQqYwHNMVgpUbxPDZrlAPrR/EC06o16UBAS9m1fMFGkixvQKyB6aM4EwMfWGZC9E/DYMJuxil36GnpQ3u+bgplFIe31STA1xUrYGP52/D/C5/xxvRiTQ7Vr6c5xQf86drSJaL/9Xbrcg9gp4wMzLYzENXEQEgmw3razcu02Mz/4VGP56SQ1EDBb/X9M89TmVV9gPHB3VPTbulqM6pFgVOsa/mBOWgVthmjY2ByDr+nhfRs7hjBPht1dTA5dVAHlruzN/CKtaUvSY6rHNtWYWCSnoPWMjApnYHHU+QB/7dQJdovhVcs95/ewDViY9dl1fYCceBe0BFVODSS5QHvuLBOtN+ya6eo3DknML4ZMzv7Ptmz9zXTOEloFY7nJzXo4X2o78VhcaJDbUeZ5PIdffheKj+agOOlJpvLzGCA5aYwSeDZNgZmXUeYgyy0IPB+Bv51PExW36tLFlKdpQScTGFGNJAjaoo1AV4wDpceEgTcjcBtLJSYhx/fi2Exfzgkaz7GLYBJzpL/LQGXVF4vFixYZT8Fr5vGywLtPYb/fgkjxR4R2BQ2BE77iYHwUgZerBwPFbp8qXFcQMAt7aYW3kLZtkTRMBZnZl/7LCQWFD6bksNOOuc43sd60+Vtek8h74nTHHDJb8poS+RlaTM1Ud0WAtbTccbdkq2rhGFxdYvFeBvdwXqUVjuXZlsZOG0+BPktmTApDWNyFioXlc2FOhdsIcKe6wvr0rfqVR48xIisBgIGm90qG5i8GtOFX3kjAzPxa49SY0RoQbWyr7TaFRszoUh7CJ4oZj3oUjh6NPQkwhZ5elYKmJZ1ZLURcOd1s0ZgSGz22DeQR52wl/G1gX0lcOcr9yyyjuv8rBtMKevVaaXCsEJDgs6CxErA9Ze01aKTzhl7jSxwVDMLSGB8irrKwPRyxuldISg+0aQr7zolyKHW1xPwLwT8I+2KRE8CYIJ93R/D/I7hPV7kVSP7Sl83TSg5oHPKhsOOpg8lw9ppTS4BHyPgL2h7J8eWtYQJw7qAr8gH/ndNGJjsBll9EyOxEvAzywpiJCvUW85DVEOgNHAjO07lDIkXK8fJ3hIsK4gl4HkEfGfYwVstBluX+NZSu04c1jWGMWpEVMsfwxU66TwHhbTw9NstyBrk2q1lHLqyQ3wtb1PJA+aGBUUIOdCH2rZIAlNOBBm/7729jF547K+ilZY0j5EF7IoUFEnkQO9sSpAE5tJaM3oDB6DK85oz+GOg/Ybg+F3a/Ci8pfGcjLSAUESZXsYuGELAm6+uEIUt0hwBLgMa4H6mmzUv98/O47rH8d6uhb3azyCx423YemMF7O5MgBz9dqgw54Ed2HSAqnEor6dJcbXD4K2fIyH8jCfwVw1viB73nzs6joDnCB3z939TudKrHNmuzjWCk/A77SfOMhf0Z2Gf5gt496IKFlc9Bk+VjYA9zcIhdWvNRwSbKZaXuCck9SaNnOOSu9kcFligDvYAXtAUDBYH/6KgtbWDvps/ZVvdUQJT0m5uR6Z7pTI/carsBxze5H6rzYUewPRMqRlsOng650/k3blyk4GbKLfmjR3WJfbAZumSvGqDjmzIsE1p9vLno1f3etXh9hvvOeWNnWhKc2XmhyrND0+YkRlk0RgbYaDsmqkZKHmOfYd4m4FfSVlEu8Pud1g6ysfnR5F3V/f3yuDkrtr1fgdOqfuSYIv4csKKLmWwgftD0wbfoOsrfxldl+F1WBf29aCvbpFeeP7oeN5VsL9GVxLzf3iMvLvI1/d0ezecf9PnwJQfphXWHxeLQZgHa6Sjiq+spPUY5dbU2PZdfrm6xYYjMCln77S09xtWa7kOMVkj6Uog0t93zeuXF83tN/DK4nk0FDYMxOU4XYuVZVze5jUsnW5o/40a7HdgDnosXrQYKZ2v1Ci/MC39DiO2MW5Afn7QCzp+8fEQ6HbYFF0actez8QP2e4lewHSsyk6qXi0bmH5XQXWkLsD9AsxBj8ANf1tFe5EkLJWhslRnQH+RwgOtos02bbqFN+Rd8NSRB8m7qv72129gDjpJ7J7ko7OLCDbJF335Cvh2VC1tvt3tmHo/wdZSGb8DkymAnkibb9qE96T58WY1MvMu+qXJRF/A+hSYg15Nm3AH9/f6qUjy7vu+gvUKGC0QdRsqCHUPaiTqftToQXcwj+DN0TnajO+5uIFukUrpGX3GlRnJ1Qni2gj0CzBaAGoYKhj1MOoRIY18jokMTRuiD00drPvjs0yEWFmurWCu7QBfAo+S6LiPxq5nVo79nFmupA718bsBe6lRv8uQUCjfDwkeE5x0qIfc/qGHuWeCk869cSngXwH/Kpu26mnqCQAAAABJRU5ErkJggg=="></div>
		</div>
	</div>
</div>
