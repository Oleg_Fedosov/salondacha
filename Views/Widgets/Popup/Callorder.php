<div class="mfiModal zoomAnim wCallBackPop">
	<div class="popupTitle">заказать звонок</div>
	<div data-form="true" data-ajax="callback" class="wForm wFormDef wFormPreloader">
		<div class="wFormRow">
			<div class="wFormInput">
				<label for="nameCall">Ваше имя</label>
				<input id="nameCall" type="text" data-name="name" required="required" data-rule-minlength="2" data-rule-word="true" name="nameCall" class="wInput"/>
			</div>
			<div class="wFormInput">
				<label for="phoneCall">Ваш телефон</label>
				<input id="phoneCall" type="tel" data-name="phone" required="required" data-rule-phoneRU="true" name="callPhone" class="wInput"/>
			</div>
		</div>
		<div class="wFormRow"><span class="wCallText">Укажите Ваш номер и наш менеджер свяжется<br/>с Вами в ближайшее время!</span></div>
		<div class="wFormRow w_last">
			<button class="wSubmit wBtn w_success">
				<span>Заказать звонок</span>
			</button>
		</div>
		<?php if(array_key_exists('token', $_SESSION)): ?>
			<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
		<?php endif; ?>
		<input type="hidden" data-name="action" value="Заказать звонок" />
		<input type="hidden" data-name="itemId" value="<?php echo $itemId ?>" />
	</div>
</div>





