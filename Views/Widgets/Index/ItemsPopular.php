<div class="wSliderOther">
	<div class="wSliderTitle">
		<span>Популярные товары</span>
	</div>
	<div class="wSliderTovars wListItem">
		<ul>
			<?php foreach( $result as $obj ): ?>
				<li>
					<div class="wSliderTovarsItem">
						<a href="<?php echo Core\HTML::link($obj->alias . '/p' . $obj->id) ?>" class="wSliderTovarsTop">
							<?php if( is_file(HOST.Core\HTML::media('images/catalog/big/'.$obj->image)) ): ?>
								<img src="<?php echo Core\HTML::media('images/catalog/big/' . $obj->image); ?>" 
									alt="<?php echo $obj->name ?>" title="<?php echo $obj->name ?>">
							<?php endif ?>
						</a>
						<div class="wSliderTovarsBot">
							<a href="<?php echo Core\HTML::link($obj->alias . '/p' . $obj->id) ?>" class="wSliderName">
								<span><?php echo $obj->name ?></span>
							</a>
							<div class="wSliderPrice"><span><?php echo $obj->cost ?></span> руб</div>
							<div data-param="{&quot;item_id&quot; : &quot;<?php echo $obj->id; ?>&quot;}" class="wBtn wSliderBuy mfiB">
								<span>Купить</span>
							</div>
						</div>
					</div>
				</li>
			 <?php endforeach; ?>
		</ul>
	</div>
	<div class="wSlidersPagg">
		<div class="wPrev">
			<svg>
				<use xlink:href="#icon_prev"/>
			</svg>
		</div>
		<div class="wNext">
			<svg>
				<use xlink:href="#icon_next"/>
			</svg>
		</div>
	</div>
</div>
	