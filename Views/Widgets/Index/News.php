<div class="wIndexNews">
	<div class="wSize">
		<div class="wSliderTitle"><span>последние новости</span></div>
		<div class="wNewsList">
			<?php foreach ($result as $obj): ?>
				<div class="wNewsItem">
					<a href="<?php echo Core\HTML::link('news/' . $obj->alias, true); ?>" class="wNewsItemImg">
						<?php if ( is_file( HOST.Core\HTML::media('images/news/small/'.$obj->image)) ): ?>
							<img src="<?php echo Core\HTML::media('images/news/small/'.$obj->image); ?>"
								alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>">
						<?php endif ?>
					</a>					
					<div class="wNewsItemCont">
						<div class="wNewsItemDate"><?php echo date( 'd.m.Y', $obj->date ); ?></div>
						<a href="<?php echo Core\HTML::link('news/' . $obj->alias, true); ?>" class="wNewsItemName"><?php echo $obj->name; ?></a>
						<div class="wNewsItemText"><?php echo Core\Text::limit_words( strip_tags($obj->text), 20 ); ?></div>
						<a href="<?php echo Core\HTML::link('news/' . $obj->alias, true); ?>" class="wNewsItemLink"><span>Подробнее</span> →</a>
					</div>					
				</div>
			<?php endforeach; ?>
			<div class="w_clear"></div>
		</div>
		<div class="wAllNews">
			<a href="<?php echo Core\HTML::link('news', true); ?>" class="wBtn">
				<span>смотреть все</span>
			</a>
		</div>
	</div>
</div>