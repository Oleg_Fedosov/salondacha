<div class="wMainSlider">
	<div class="wMainSliderBlock">
		<ul>
			<?php foreach ( $result as $obj ): ?>
				<li>
					<?php if ( is_file(HOST.Core\HTML::media('images/slider/big/'.$obj->image))): ?>
						<img src="<?php echo Core\HTML::media('images/slider/big/'.$obj->image); ?>" alt="" title="">
					<?php endif; ?>
					<?php if ($obj->name): ?>
						<div class="wMainSliderText">
							<div class="wSize"><?php echo $obj->name; ?></div>
						</div>
					 <?php endif ?>
				</li>
			<?php endforeach ?>
		</ul>
	</div>
	<div class="wPrev">
		<svg>
			<use xlink:href="#icon_prev"/>
		</svg>
	</div>
	<div class="wNext">
		<svg>
			<use xlink:href="#icon_next"/>
		</svg>
	</div>
</div>

