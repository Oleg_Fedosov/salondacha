<div class="wIndexPreim">
	<div class="wSize">
		<div class="wSliderTitle wWhite">
			<span>наши преимущества</span>
		</div>
		<div class="wPreimList">
			<div class="wPreimItem">
				<div class="wPreimSvgBlock">
					<svg>
						<use xlink:href="#icon_pre1"/>
					</svg>
				</div><span><?php echo Core\Config::get('static.advantages1'); ?></span>
			</div>
			<div class="wPreimItem">
				<div class="wPreimSvgBlock">
					<svg>
						<use xlink:href="#icon_pre2"/>
					</svg>
				</div><span><?php echo Core\Config::get('static.advantages2'); ?></span>
			</div>
			<div class="wPreimItem">
				<div class="wPreimSvgBlock">
					<svg>
						<use xlink:href="#icon_pre3"/>
					</svg>
				</div><span><?php echo Core\Config::get('static.advantages3'); ?></span>
			</div>
			<div class="wPreimItem">
				<div class="wPreimSvgBlock">
					<svg>
						<use xlink:href="#icon_pre4"/>
					</svg>
				</div><span><?php echo Core\Config::get('static.advantages4'); ?></span>
			</div>
		</div>
	</div>
</div>