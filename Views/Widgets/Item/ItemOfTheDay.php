<a href="<?php echo Core\HTML::link($obj->alias . '/p' . $obj->id) ?>" class="wDayTovar">
	<?php if( is_file(HOST.Core\HTML::media('images/catalog/big/'.$obj->image)) ): ?>
		<img src="<?php echo Core\HTML::media('images/catalog/big/' . $obj->image); ?>" alt="<?php echo $obj->name ?>"
			title="<?php echo $obj->name ?>">
	<?php endif ?>
	<div class="wDayTovarTitle">
		<svg>
			<use xlink:href="#icon_star"/>
		</svg>
		<span>товар дня</span>
	</div>
	<div class="wDayName">
		<span><?php echo $obj->name ?></span>
	</div>
</a> 