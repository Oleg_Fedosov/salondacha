<header class="wHeader">
	<div class="wHeaderFix">
	<a href="#mainMenu" class="wFixIcons wMenuFix">
		<svg>
			<use xlink:href="#icon_menu"/>
		</svg><span>Меню</span>
	</a>
		<div class="wFixIcons wSearchFixBtn">
			<svg>
				<use xlink:href="#icon_find"/>
			</svg><span>Поиск</span>
		</div>
		<a href="#catalogMenu" class="wFixIcons wCatBtn">
			<svg><use xlink:href="#icon_catalog"/>
			</svg>
			<span>Каталог</span></a>
		<div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;callback&quot;}" class="wFixIcons wPhoneFix mfiA">
			<svg><use xlink:href="#icon_phone"/></svg>
			<span>Звонок</span>
		</div>
		<div data-param="{&quot;action&quot;:&quot;default&quot;,&quot;id&quot;:&quot;123&quot;}" class="wFixIcons wBasketFix mfiB">
			<svg>
				<use xlink:href="#icon_cart"/>
			</svg><span>Корзина</span>
		</div>
	</div>
	<div class="wHeaderTop">
		<div class="wSize">
			<?php if (Core\Route::controller() == 'index') : ?>				
				<span class="wLogo wHeaderLH">
					<img src="<?php echo Core\HTML::media('pic/logo.png'); ?>" alt="">
				</span>
			<?php else: ?>
				<a class="wLogo wHeaderLH" href="<?php echo Core\HTML::link('', true); ?>">
					<img src="<?php echo Core\HTML::media('pic/logo.png'); ?>">
				</a>   
			<?php endif; ?> 
			<div class="wPlace wHeaderLH">
				<div class="wPlaceSvg w_fll">
					<svg>
						<use xlink:href="#icon_place"/>
					</svg>
				</div>
				<div class="wPlaceText w_flr">
					<span>
						<?php echo nl2br(Core\Config::get('static.office_address')); ?>
					</span>
				</div>
				<div class="w_clear"></div>
			</div>
			<div class="wPhonesTop wHeaderLH">
				<?php if ($phone1 = trim(Core\Config::get('static.phone1'))): ?>
					<a href="tel:+<?php echo preg_replace('#\D#', '', $phone1); ?>?call"><?php echo $phone1; ?></a>
				<?php endif ?>
				<?php if ($phone2 = trim(Core\Config::get('static.phone2'))): ?>
					<a href="tel:+<?php echo preg_replace('#\D#', '', $phone2); ?>?call"><?php echo $phone2; ?></a>
				<?php endif ?>
			</div>
			<div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;callback&quot;}"
				class="wCallBack wHeaderLH mfiA">
				<div class="wCallBackSvg w_fll">
					<svg>
						<use xlink:href="#icon_phone"/>
					</svg>
				</div>
				<div class="wCallBackText w_flr"><span>Обратный звонок</span></div>
				<div class="w_clear"></div>
			</div>
			<div data-param="{&quot;action&quot;:&quot;default&quot;}" class="wCart wHeaderLH mfiB">
				<div class="wCartSvg w_fll">
					<svg>
						<use xlink:href="#icon_cart"/>
					</svg>
				</div>
				<div class="wCartText w_flr">
					<p class="wCartItems">Товаров: <span><?php echo $countItemsInTheCart; ?></span></p>
					<p class="wCartSum">Сумма: <span><?php echo $sumItemsInTheCart; ?></span> руб.</p>
				</div>
				<div class="w_clear"></div>
			</div>
		</div>
	</div>
	<div class="wHeaderBot">
		<div class="wSize">
			<div class="wMenuMain w_fll">
				<nav class="wMenu">
					<ul>
						<?php foreach ($contentMenu as $key => $obj): ?>
						<?php if (($obj->url != '/' && strpos($_SERVER['REQUEST_URI'], $obj->url) !== false)
                                    || ($obj->url == '/' && Core\Route::controller() == 'index')): ?>
							<li class="w_active">
							<?php else: ?>
                            <li>
							<?php endif; ?>
								<a href="<?php echo Core\HTML::link($obj->url, true); ?>">
									<span><?php echo $obj->name; ?></span>
								</a>
							</li>
						<?php endforeach ?>						
					</ul>
				</nav>
				<!-- wMenu -->
			</div>
			<div class="wSearch w_flr">
				<form data-form="true" class="wForm wFormDef wFormSearch" action="/search" method="GET">
					<div class="wFormRow">
						<svg class="wSearchIcon">
							<use xlink:href="#icon_find"/>
						</svg>
						<input type="text" required name="search" placeholder="Введите название товара" data-rule-minlength="2" class="wInput">
					</div>
					<button class="wSubmit wBtn w_success"><span>Найти</span></button>
				</form>
			</div>
			<div class="w_clear"></div>
		</div>
	</div>
</header>
