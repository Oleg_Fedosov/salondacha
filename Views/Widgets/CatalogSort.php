<form class="wFilter" id="catalogSort"  data-form="true" action="<?php echo str_replace('/page/' . Core\Route::param('page'), '', Core\Arr::get($_SERVER, 'REQUEST_URI')) ?>" method="GET">
        <div class="wSort w_fll">
            <div class="wSortText">Сортировка:</div>
            <div class="wSortSelect">
                <select name="sort" class="wSelect2">
                    <option value="cost_asc" <?php echo (Core\Arr::get($_GET, 'sort') == 'cost_asc') ? 'selected' : ''; ?>>По возростанию цены</option>
                    <option value="cost_desc" <?php echo (Core\Arr::get($_GET, 'sort') == 'cost_desc') ? 'selected' : ''; ?>>По убыванию цены</option>
                    <option value="name_asc" <?php echo (Core\Arr::get($_GET, 'sort') == 'name_asc')  ? 'selected' : ''; ?>>От А до Я</option>
                    <option value="name_desc" <?php echo (Core\Arr::get($_GET, 'sort') == 'name_desc')  ? 'selected' : ''; ?>>От Я до А</option>
                </select>
            </div>
            <div class="wSortReset">
				<a href="<?php echo (strstr(Core\Arr::get($_SERVER, 'REQUEST_URI'), '/page/', true)) ? : (strstr(Core\Arr::get($_SERVER, 'REQUEST_URI'), '?' , true)) . (Core\Arr::get($_GET, 'search') ? '?search=' . Core\Arr::get($_GET, 'search') : null) ?>">
					<span>Сбросить всё</span>
				</a>
			</div>
        </div>
    <div class="wNumeric w_flr">
		<div class="wNumericText">Выводить по:</div>
			<div class="wNumericList">
				<?php $limit = Core\Config::get('basic.limit'); ?>
				<?php for( $i = $limit; $i < $limit * 5; $i += $limit ): ?>
					<div class="ck-button">
						<label>
						  <input type="radio" name="per_page" value="<?php echo $i; ?>" <?php echo (Core\Arr::get($_GET, 'per_page') == $i) ? 'checked' : ''; ?>><span><?php echo $i; ?></span>
						</label>
					</div>	
				<?php endfor; ?>	
			</div>
		</div>
    <div class="w_clear"></div>
	<?php if (Core\Arr::get($_GET, 'search')): ?>
		<input type="hidden" name="search" value="<?php echo Core\Arr::get($_GET, 'search')?>">
	<?php endif ?>
</form>

<script>
    $(function(){
	$('#catalogSort select, input[name=per_page]').on('change', function(){
			
			var form = $("#catalogSort");
			form.submit();

        });
    });
</script>