<footer class="wFooter">
    <div class="wFooterTop">
		<div class="wSize">
			<div class="wLogoBotBlock w_fll">
				<?php if (Core\Route::controller() == 'index') : ?>				
					<span class="wLogoBot">
						<img src="<?php echo Core\HTML::media('pic/logo.png'); ?>" alt="">
					</span>
				<?php else: ?>
					<a class="wLogoBot" href="<?php echo Core\HTML::link('', true); ?>">
						<img src="<?php echo Core\HTML::media('pic/logo.png'); ?>">
					</a>   
				<?php endif; ?> 
			</div>
			<div class="wFooterContacts w_flr">
				<div class="wPhonesBot">
				<?php if ($phone1 = trim(Core\Config::get('static.phone1'))): ?>
					<a href="tel:+<?php echo preg_replace('#\D#', '', $phone1); ?>?call"><?php echo $phone1; ?></a>
				<?php endif ?>
				<?php if ($phone2 = trim(Core\Config::get('static.phone2'))): ?>
					<a href="tel:+<?php echo preg_replace('#\D#', '', $phone2); ?>?call"><?php echo $phone2; ?></a>
				<?php endif ?>
				</div>
				<div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;callback&quot;}"
					class="wCallBack wHeaderLH mfiA">
					<div class="wCallBackSvg w_fll">
						<svg>
							<use xlink:href="#icon_phone"/>
						</svg>
					</div>
					<div class="wCallBackText w_flr">
						<span>Обратный звонок</span>
					</div>
					<div class="w_clear"></div>
				</div>
				<div class="wPlace wHeaderLH">
					<div class="wPlaceSvg w_fll">
						<svg>
							<use xlink:href="#icon_place"/>
						</svg>
					</div>
					<div class="wPlaceText w_flr"><span><?php echo nl2br(Core\Config::get('static.office_address')); ?></span></div>
					<div class="w_clear"></div>
				</div>
			</div>
			<div class="wFooterList">
				<ul>
					<?php foreach ($categories as $obj): ?>
						<li>
							<a href="<?php echo Core\HTML::link('catalog/' . $obj->alias, true) ?>"><?php echo $obj->name ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="w_clear"></div>
		</div>
	</div>
	<div class="wFooterBot">
		<div class="wSize">
			<div class="wCoryWryte w_fll"><?php echo Core\Config::get('static.copyright'); ?></div>
			<div class="wWezom w_flr"><a href="http://wezom.com.ua" target="_blank">
					<svg>
						<use xlink:href="#icon_wezom"/>
					</svg><span>Разработка сайта - <span>студия Wezom</span></span></a></div>
			<div class="w_clear"></div>
		</div>
	</div>
	<div id="scrollerUp" class="scrollerUp">
		<svg>
			<use xlink:href="#icon_up"/>
		</svg>
	</div>
</footer>