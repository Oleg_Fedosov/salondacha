<div class="wCatalogleft">
	<ul>
		<?php foreach ($result as $obj): ?>
			<li>
				<a href="<?php echo Core\HTML::link('catalog/' . $obj->alias, true) ?>"
					<?php echo ($rootId == $obj->id) ? 'class="cur"' : null ?>>
					<svg>
						<use xlink:href="#icon_cat<?php echo $obj->id ?>"/>
					</svg><span><?php echo $obj->name ?></span>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>