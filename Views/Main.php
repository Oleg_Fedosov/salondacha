<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<head>
	<!-- (c) студия Wezom | www.wezom.com.ua-->
    <?php echo Core\Widgets::get('Head', $_seo); ?>
	    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body class="indexPage">
    <?php foreach ( $_seo['scripts']['body'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <div class="seoTxt" id="seoTxt">
        <div class="wSize">
			<div class="wTxt">
				<?php if (trim($_seo['h1'])): ?>
					<h1><?php echo $_seo['h1'] ?></h1>
				<?php endif ?>
				<?php echo $_content; ?>
			</div>
        </div>
    </div>
    <div class="wWrapper">
        <?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
		<!-- .wHeader -->
        <div class="wContainer">
			<?php echo Core\Widgets::get('Index_Slider'); ?>
            <div class="wSize">                
                <div class="wLeft">
						<?php echo Core\Widgets::get('Groups_CatalogMenuLeft'); ?>
						<?php echo Core\Widgets::get('Item_ItemOfTheDay'); ?>
					</div>
					<div class="wMiddle">
						<div class="wIndexSliders">
							<?php echo Core\Widgets::get('Index_ItemsPopular'); ?>
							<?php echo Core\Widgets::get('Index_ItemsNew'); ?>
							<?php echo Core\Widgets::get('Index_ItemsSale'); ?>
						</div>
					</div>
				</div>
				<?php echo Core\Widgets::get('Index_Advantages'); ?>
				<?php echo Core\Widgets::get('Index_News'); ?>							
			<div class="wSeoBlock">
				<div id="cloneSeo"></div>
			</div>
			<!-- .wConteiner -->
			<div class="wSize">
				<div id="cloneSeo"></div>
			</div>
        </div>
    </div>
    <?php echo Core\Widgets::get('Footer', array('counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config)); ?>
	<!-- .wFooter -->
	<?php echo Core\Widgets::get('HiddenData'); ?>
</body>
</html>