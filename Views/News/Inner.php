<div class="wMiddle">
	<h1 class="wSliderTitle"><span><?php echo $obj->h1; ?></span></h1>
	<div class="wNewsDate"><?php echo date( 'd.m.Y', $obj->date ); ?></div>
	<div class="wTxt">
	<?php if ( is_file(HOST.Core\HTML::media('images/news/small/'.$obj->image)) AND $obj->show_image ): ?>
		<img src="<?php echo Core\HTML::media('images/news/small/'.$obj->image); ?>" alt="">
	<?php endif; ?>
	<?php echo $obj->text; ?>	
	</div><a href="<?php echo Core\HTML::link('news'); ?>" class="wBack wBtn"><span>Вернуться к списку новостей</span></a>
</div>

