<div class="wMiddle">
	<div class="wSliderTitle"><span>Новости</span></div>			
	<div class="wNewsList">
		<?php foreach ( $result as $obj ): ?>					
			<div class="wNewsItem">
				<?php if ( is_file( HOST.Core\HTML::media('images/news/small/'.$obj->image)) ): ?>
					<a href="<?php echo Core\HTML::link('news/'.$obj->alias, true); ?>" class="wNewsItemImg">
						<img src="<?php echo Core\HTML::media('images/news/small/'.$obj->image, true); ?>" 
							 alt="<?php echo $obj->name ?>" title="<?php echo $obj->name ?>">
					</a>
				<?php endif ?>
				<div class="wNewsItemCont">
					<div class="wNewsItemDate"><?php echo date( 'd.m.Y', $obj->date ); ?></div>
					<a href="<?php echo Core\HTML::link('news/'.$obj->alias, true); ?>" class="wNewsItemName"><?php echo $obj->name ?></a>
					<div class="wNewsItemText"><?php echo Core\Text::limit_words(strip_tags($obj->text), 100, '...') ?></div>
					<a href="<?php echo Core\HTML::link('news/'.$obj->alias, true); ?>" class="wNewsItemLink"><span>Подробнее</span> →</a>
				</div>
			</div>
		<?php endforeach; ?>																
	</div>
	<?php echo $pager; ?>
</div>



