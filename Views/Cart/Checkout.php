<?php if( count($cart) ): ?>
	<div class="wOrderLeft w_fll">
		<div class="wBasketSteps">
			<a href="<?php echo \Core\HTML::link('/cart', true) ?>" class="wBasketStep">
				<span>Персональные данные</span>
			</a>
			<a href="<?php echo \Core\HTML::link('/checkout', true) ?>" class="wBasketStep cur">
				<span>Доставка и оплата</span>
			</a>
		</div>
		<div data-form="true" data-ajax="checkout" class="wForm wFormDef wOrderForm">
			<div class="wFormRow wDuoRow">
				<div class="wFormInput w_fll">
					<label for="townO">Укажите город</label>
					<input type="text" data-name="city" data-rule-word="true" data-rule-minlength="2" name="townO" required id="townO" class="wInput">
				</div>
				<div class="wFormInput w_fll">
					<label for="dosO">Способ доставки</label>
					<select data-name="delivery" name="dosO" required id="dosO" class="wSelect2">
						<option value=""></option>
						<?php if(count($delivery)): ?>
							<?php foreach( $delivery as $id => $name ): ?>
								<option value="<?php echo $id; ?>"><?php echo $name; ?></option>
							<?php endforeach ?>
						<?php endif ?>
					</select>
				</div>
				<div class="w_clear"></div>
			</div>
			<div class="wFormRow">				
				<span>Способ оплаты</span>
				<?php foreach($payment as $id => $name): ?>										
					<div class="wRadio">
						<label>
							<input type="radio" data-name="payment" value="<?php echo $id; ?>" required name="radio"><ins>&nbsp;</ins><span>
							<span><?php echo $name; ?></span> 
							<br> <?php echo Core\Config::get('static.paymentInfo' . $id); ?></span>
						</label>
					</div>					
				<?php endforeach ?>				
			</div>
			<div class="wFormRow">
				<a href="<?php echo \Core\HTML::link('/cart', true) ?>" class="wBtn w_fll">
				<span>назад</span>
			</a>
				<div class="wBtn w_flr wSubmit w_success">
					<svg>
						<use xlink:href="#icon_nal"/>
					</svg><span>подтверждаю заказ</span>
				</div>
				<?php if(array_key_exists('token', $_SESSION)): ?>
					<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
				<?php endif; ?>
				<div class="w_clear"></div>
			</div>
		</div>
	</div>
	<div class="wOrderCart w_flr">
		<div class="wCartTitle">ТОвары в корзине</div>
		<div class="wBasketList">
			<?php foreach ($cart as $key => $item): ?>
				<?php $obj = Core\Arr::get( $item, 'obj' ); ?>
				<?php if( $obj ): ?>
					<div class="wBasketItem">
						<div class="wBasketImg w_fll">
							<a href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" target="_blank" class="wBasketPhoto">
								<?php if( is_file(HOST.Core\HTML::media('images/catalog/medium/'.$obj->image)) ): ?>
									<img src="<?php echo Core\HTML::media('images/catalog/medium/'.$obj->image); ?>" 
										alt="<?php echo $obj->name ?>" title="<?php echo $obj->name ?>">
								<?php endif; ?>
							</a>
						</div>
						<div class="wBasketText w_ovh">
							<a href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" 
								target="_blank" class="wBasketLink">
								<?php echo $obj->name ?>
							</a>
							<div class="wCartItemCost">
								<span class="wKollBasket"><?php echo $item['count']; ?></span>шт. х <span><?php echo $obj->cost ?></span> руб
							</div>
							<div class="wItemPrice">
								<span><?php echo $cost = $item['count'] * $obj->cost; $amount += $cost; ?></span> руб
							</div>
						</div>
						<div class="w_clear"></div>
					</div>
				<?php endif ?>
			<?php endforeach ?>
		</div>
		<div class="wBasketTotal">Итого: 
			<span>
				<span><?php echo $amount; ?></span> руб
			</span>
		</div>
		<div data-param="{&quot;action&quot;:&quot;default&quot;,&quot;id&quot;:&quot;123&quot;}" class="wBtn mfiB w_flr">
			<span>Редактировать заказ</span>
		</div>
		<div class="w_clear"></div>
	</div>
	<div class="w_clear"></div>
<?php else: ?>
	<p class="emptyCartBlock">Ваша корзина пуста. <a href="<?php echo Core\HTML::link('products'); ?>">Начните делать покупки прямо сейчас!</a></p>
<?php endif ?>