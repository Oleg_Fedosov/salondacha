<?php if( count($cart) ): ?>
	<div class="wOrderLeft w_fll">
		<div class="wBasketSteps">
			<a href="<?php echo \Core\HTML::link('/cart', true) ?>" class="wBasketStep cur">
				<span>Персональные данные</span>
			</a>
			<a href="<?php echo \Core\HTML::link('/checkout', true) ?>" class="wBasketStep">
				<span>Доставка и оплата</span>
			</a>
		</div>
		<div data-form="true" data-ajax="firstCheckout" class="wForm wFormDef wOrderForm" >
			<div class="wFormRow wDuoRow">
				<div class="wFormInput w_fll">
					<label for="nameO">Ваше имя</label>
					<input type="text" data-rule-word="true" data-rule-minlength="2" data-name = "name" name="nameO" 
					value="<?php echo $_SESSION['name'] ? : null; ?>" required id="nameO" class="wInput">
				</div>
				<div class="wFormInput w_fll">
					<label for="mailO">Ваше Email</label>
					<input type="email" data-rule-email="true" data-name = "email" name="mailO" required id="mailO" 
					value="<?php echo $_SESSION['email'] ? : null; ?>" class="wInput" >
				</div>
				<div class="w_clear"></div>
			</div>
			<div class="wFormRow wDuoRow">
				<div class="wFormInput w_fll">
					<label for="phpneO">Ваше телефон</label>
					<input type="tel" data-rule-phoneru="true" data-name = "phone" name="phpneO" required id="phpneO" 
					value="<?php echo $_SESSION['phone'] ? : null; ?>" class="wInput" >
				</div>
				<div class="w_clear"></div>
			</div>
			<div class="wFormRow">
				<button class="wBtn wSubmit">
					<span>продолжить</span>
				</button>
				<?php if(array_key_exists('token', $_SESSION)): ?>
					<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
				<?php endif; ?>
			</div>
		</div>		
	</div>
	<div class="wOrderCart w_flr">
		<div class="wCartTitle">ТОвары в корзине</div>
		<div class="wBasketList">
			<?php foreach ($cart as $key => $item): ?>
				<?php $obj = Core\Arr::get( $item, 'obj' ); ?>
				<?php if( $obj ): ?>
					<div class="wBasketItem">
						<div class="wBasketImg w_fll">
							<a href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" target="_blank" class="wBasketPhoto">
								<?php if( is_file(HOST.Core\HTML::media('images/catalog/medium/'.$obj->image)) ): ?>
									<img src="<?php echo Core\HTML::media('images/catalog/medium/'.$obj->image); ?>" 
										alt="<?php echo $obj->name ?>" title="<?php echo $obj->name ?>">
								<?php endif; ?>
							</a>
						</div>
						<div class="wBasketText w_ovh">
							<a href="<?php echo Core\HTML::link($obj->alias.'/p'.$obj->id); ?>" 
								target="_blank" class="wBasketLink" style="display:block">
								<?php echo $obj->name ?>
							</a>
							<div class="wCartItemCost">
								<span class="wKollBasket"><?php echo $item['count']; ?></span>шт. х <span><?php echo $obj->cost ?></span> руб
							</div>
							<div class="wItemPrice">
								<span><?php echo $cost = $item['count'] * $obj->cost; $amount += $cost; ?></span> руб
							</div>
						</div>
						<div class="w_clear"></div>
					</div>
				<?php endif ?>
			<?php endforeach ?>
		</div>
		<div class="wBasketTotal">Итого: 
			<span>
				<span><?php echo $amount; ?></span> руб
			</span>
		</div>
		<div data-param="{&quot;action&quot;:&quot;default&quot;,&quot;id&quot;:&quot;123&quot;}" class="wBtn mfiB w_flr">
			<span>Редактировать заказ</span>
		</div>
		<div class="w_clear"></div>
	</div>
	<div class="w_clear"></div>
<?php else: ?>
	<p class="emptyCartBlock">Ваша корзина пуста. <a href="<?php echo Core\HTML::link('catalog'); ?>">Начните делать покупки прямо сейчас!</a></p>
<?php endif ?>