<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<head>
	<!-- (c) студия Wezom | www.wezom.com.ua-->
    <?php echo Core\Widgets::get('Head', $_seo); ?>
</head>
<body class="indexPage">
    <?php foreach ( $_seo['scripts']['body'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <div class="seoTxt" id="seoTxt">
        <div class="wSize">
			<div class="wTxt">
				<?php if (trim($_seo['h1'])): ?>
					<h1><?php echo $_seo['h1'] ?></h1>
				<?php endif ?>
				<?php echo $_content; ?>
			</div>
        </div>
    </div>
    <div class="wWrapper">
        <?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
		<!-- .wHeader -->
        <div class="wContainer"> 
			<div class="<?php echo Core\Config::get('content_class'); ?>">
				<div class="wSize">
					<?php echo $_breadcrumbs; ?>				
					<!-- breadcrumbs -->
					<div class="wLeft">
						<?php echo Core\Widgets::get('Groups_CatalogMenuLeft'); ?>
						<?php echo Core\Widgets::get('Item_ItemOfTheDay'); ?>									
					</div>
					<?php echo $_filing; ?>
				</div>					
			</div>
			<?php echo Core\Widgets::get('Index_Advantages'); ?>
			<div class="wSeoBlock">
				<div id="cloneSeo"></div>
			</div>
			<!-- .wConteiner -->
			<div class="wSize">
				<div id="cloneSeo"></div>
			</div>
        </div>
    </div>
    <?php echo Core\Widgets::get('Footer', array('counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config)); ?>
		<!-- .wFooter -->
	<?php echo Core\Widgets::get('HiddenData'); ?>
</body>
</html>