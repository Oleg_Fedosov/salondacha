<div class="wMiddle">
	<h1 class="wSliderTitle"><span><?php echo $page->h1 ? : $page->name ; ?></span></h1>
	<div class="wTxt">
		<?php echo $page->text; ?>
	</div>
	<?php if ( count( $kids ) ): ?>
		<ul class="faq_block wTxt">
			<?php foreach ( $kids as $obj ): ?>
				<li>
					<a href="<?php echo Core\HTML::link($obj->alias); ?>"><?php echo $obj->name; ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</div>