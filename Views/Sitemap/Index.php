<div class="wSliderTitle">
	<span>Карта сайта</span>
</div>
<nav class="wSitemap">
	<ul>
		<li>
			<a href="<?php echo Core\HTML::link('/'); ?>">
				<span>Главная</span>
			</a>
		</li>
		<li><a href="<?php echo Core\HTML::link('catalog'); ?>">Каталог</a>
			<?php echo Core\View::tpl(array('result' => $groups, 'cur' => 0, 'add' => '/catalog'), 'Sitemap/Recursive'); ?>
		</li>
		<?php foreach($pages[0] AS $obj): ?>
			<li><a href="<?php echo Core\HTML::link($obj->alias); ?>"><span><?php echo $obj->name; ?></span></a>
				<?php echo Core\View::tpl(array('result' => $pages, 'cur' => $obj->id, 'add' => ''), 'Sitemap/Recursive'); ?>
			</li>
		<?php endforeach; ?>
		<li class="haveSubMenu"><a href="<?php echo Core\HTML::link('news'); ?>">Новости</a>
        <?php if(count($news)): ?>
            <ul>
                <?php foreach($news AS $obj): ?>
                    <li ><a href="<?php echo Core\HTML::link('news/'.$obj->alias); ?>"><?php echo $obj->name; ?></a></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </li>
	<li>
		<a href="<?php echo Core\HTML::link('contact'); ?>">
			<span>Контакты</span>
		</a>
	</li>
	</ul>
</nav>

