<?php foreach($result as $obj): ?>
	<li>
		<div class="wCheck mfiB" data-param="{&quot;item_id&quot;:&quot;<?php echo $obj->id ?>&quot;}">
			<label>
				<input type="checkbox" name="radio"><ins>&nbsp;</ins><span></span>
			</label>
		</div>
			<a href="<?php echo Core\HTML::link($obj->alias . '/p' . $obj->id) ?>" target="_blank" class="wCartSliderImg">
				<?php if( is_file(HOST . Core\HTML::media('images/catalog/medium/' . $obj->image)) ): ?>
					<?php if($obj->sale): ?>
						<div class="wFlag">
							<img src="<?php echo Core\HTML::media('pic/sale.png', true) ?>" alt="">
						</div>
					<?php endif ?>
					<?php if($obj->new): ?>
						<div class="wFlag">
							<img src="<?php echo Core\HTML::media('pic/new.png', true) ?>" alt="">
						</div>
					<?php endif ?>
					<?php if($obj->recommend): ?>
						<div class="wFlag">
							<img src="<?php echo Core\HTML::media('pic/rek.png', true) ?>" alt="">
						</div>
					<?php endif ?>
					<img src="<?php echo Core\HTML::media('images/catalog/medium/' . $obj->image, true) ?>" 
						alt="<?php echo $obj->name ?>" title="<?php echo $obj->name ?>">
					<?php else: ?>
					<img src="<?php echo Core\HTML::media('pic/no-image.png', true); ?>" alt="">
				<?php endif ?>
			</a>
			<a href="<?php echo Core\HTML::link($obj->alias . '/p' . $obj->id) ?>" target="_blank" class="wCartSliderName">
				<span><?php echo $obj->name; ?></span>
			</a>
		<div class="wCartSliderPrice">
			<span><?php echo $obj->cost; ?></span> руб
		</div>
	</li>
<?php endforeach; ?>