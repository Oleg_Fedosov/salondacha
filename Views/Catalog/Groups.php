<div class="wSliderTitle"><span><?php echo $name; ?></span></div>
<div class="wCatalogList">
	<ul>
		<?php foreach ($result as $obj): ?>
			<li class="w_fll">
				<a href="<?php echo Core\HTML::link('catalog/' . $obj->alias, true); ?>" class="wCatalogLink">
					<div class="wCatalogCircle">
						<div class="wCatalogCircleInn">
							<div class="wCatalogPhoto">
								<?php if( is_file(HOST.Core\HTML::media('images/catalog_tree/'.$obj->image)) ): ?>
									<img src="<?php echo Core\HTML::media('images/catalog_tree/'.$obj->image, true); ?>" alt="">
								<?php else: ?>
									<img src="<?php echo Core\HTML::media('pic/no-image.png', true); ?>" alt="" title="">
								<?php endif; ?>
							</div>
							<div class="wCatalogSvg">
								<svg>
									<use xlink:href="#icon_cat<?php echo $obj->id; ?>"/>
								</svg>
							</div>
						</div>
					</div>
					<div class="wCatalogText">
						<span><?php echo $obj->name; ?></span>
					</div>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
	<?php echo $pager; ?>
	<div class="w_clear"></div>
</div>
	

