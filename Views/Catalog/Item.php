<div class="wRight">
	<div class="wSliderTitle wNoAfter"><span><?php echo $obj->name ?></span></div>
	<div class="wItemMainBlock">
		<div class="wItemTop">
			<?php if ($obj->artikul): ?>
				<div class='wItemTopElem'>Артикул: <span><?php echo $obj->artikul ?></span></div>
			<?php endif ?>
			<?php if ($obj->brand): ?>
				<div class='wItemTopElem'>Производитель: <span><?php echo $obj->brand ?></span></div>
			<?php endif ?>
			<?php if ($obj->model): ?>
				<div class='wItemTopElem'>Модель: <span><?php echo $obj->model ?></span></div>
			<?php endif ?>
		</div>
		<div class="wItemMain">
			<div class="wItemPhotos w_fll">
				<div class="wItemPhotosBlock">
					<div class="wItemMainPhoto">
						<ul>
							<?php $i = 0; foreach( $images as $im ): ?>
								<?php if( is_file(HOST.Core\HTML::media('images/catalog/big/'.$im->image)) ): ?>
									<li id="itemM<?php echo $i++; ?>">
										<div href="<?php echo Core\HTML::media('images/catalog/big/' . $im->image); ?>" class="mfiG">
											<img src="<?php echo Core\HTML::media('images/catalog/big/' . $im->image); ?>" alt="">
										</div>
									</li>
								<?php endif; ?>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="wItemDopPhoto">
						<div class="wItemDopSlider">
							<ul>
							<?php $i = 0; foreach( $images as $im ): ?>
								<?php if( is_file(HOST.Core\HTML::media('images/catalog/small/'. $im->image)) ): ?>
									<li>
										<a data-img="<?php echo Core\HTML::media('images/catalog/big/' . $im->image); ?>" href="#itemM<?php echo $i++; ?>" class="wItemDopImg <?php echo ($i == 1) ? 'cur' : null  ?>" >
											<img src="<?php echo Core\HTML::media('images/catalog/small/' . $im->image); ?>" alt="">
										</a>
									</li>
								<?php endif; ?>
							<?php endforeach; ?>
							</ul>	
						</div>
						<div class="wPrevItem">
							<svg>
								<use xlink:href="#icon_u"/>
							</svg>
						</div>
						<div class="wNextItem">
							<svg>
								<use xlink:href="#icon_d"/>
							</svg>
						</div>
					</div>
				</div>
			</div>
			<div class="wItemInfo w_flr">
				<div class="wItemNal">
					<svg>
						<use xlink:href="#icon_nal"/>
					</svg><span>Есть в наличии</span>
				</div>
				<div class="wItemPrice"><span><?php echo $obj->cost ?></span> руб</div>
				<div data-param="{&quot;item_id&quot; : &quot;<?php echo $obj->id; ?>&quot;}" class="wBtn w_success mfiB">
					<svg>
						<use xlink:href="#icon_cart"/>
					</svg><span>купить</span>
				</div>
				<div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" 
					data-param="{&quot;template&quot;:&quot;callorder&quot;,&quot;itemId&quot;:&quot;<?php echo $obj->id ?>&quot;}" 
					class="wBtn mfiA">
					<svg>
						<use xlink:href="#icon_phone"/>
					</svg><span>заказать звонок</span>
				</div>
				<div class="wItemInfoText"><?php echo Core\Config::get('static.communication') ?></div>
			</div>
			<div class="w_clear"></div>
		</div>
		<div class="wItemSoc">
			<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
			<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
			<div data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter" class="ya-share2"></div>
		</div>
	</div>
	<div class="wOpusItem">
		<div class="wSliderTitle wBold"><span>Описание товара</span></div>
		<div class="wTxt">
			<?php echo $obj->spec ?>
		</div>
	</div>
	<?php if ($obj->tech_spec): ?>
		<div class="wKharakteristicItem">
			<div class="wSliderTitle wBold"><span>технические харрактеристики</span></div>
			<div class="wTxt">
				<?php echo $obj->tech_spec ?>
			</div>
		</div>
	<?php endif; ?>
	<?php echo Core\Widgets::get('Item_ItemsSame'); ?>
</div>
