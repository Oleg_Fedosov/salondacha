<div class="wMiddle">
	<div class="wSliderTitle"><span><?php echo $name ?></span></div>
	<?php echo Core\Widgets::get('CatalogSort'); ?>
	<?php if( !count($result) ): ?>
		<p>По заданым параметрам товаров нет</p>
		<?php else: ?>		
		<div class="wListItem wListItems">		
			<ul>
				<?php foreach($result as $obj): ?>
					<li> 
						<div class="wSliderTovarsItem">
							<a href="<?php echo Core\HTML::link($obj->alias . '/p' . $obj->id) ?>" class="wSliderTovarsTop">
								<?php if( is_file(HOST . Core\HTML::media('images/catalog/medium/' . $obj->image)) ): ?>
									<?php if($obj->sale): ?>
										<div class="wFlag">
											<img src="<?php echo Core\HTML::media('pic/sale.png', true) ?>" alt="">
										</div>
									<?php endif ?>
									<?php if($obj->new): ?>
										<div class="wFlag">
											<img src="<?php echo Core\HTML::media('pic/new.png', true) ?>" alt="">
										</div>
									<?php endif ?>
									<?php if($obj->recommend): ?>
										<div class="wFlag">
											<img src="<?php echo Core\HTML::media('pic/rek.png', true) ?>" alt="">
										</div>
									<?php endif ?>
									<img src="<?php echo Core\HTML::media('images/catalog/medium/' . $obj->image, true) ?>" 
										alt="<?php echo $obj->name ?>" title="<?php echo $obj->name ?>">
									<?php else: ?>
									<img src="<?php echo Core\HTML::media('pic/no-image.png', true); ?>" alt="">
								<?php endif ?>
							</a>
							<div class="wSliderTovarsBot">
							<a href="<?php echo Core\HTML::link($obj->alias . '/p' . $obj->id) ?>" class="wSliderName">
								<span><?php echo $obj->name ?></span>
							</a>
							<div class="wSliderPrice">
								<span><?php echo $obj->cost; ?></span> руб
							</div>
								<div data-param="{&quot;item_id&quot; : &quot;<?php echo $obj->id; ?>&quot;}" class="wBtn wSliderBuy mfiB">
								<span>Купить</span>
							</div>
							</div>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php echo $pager; ?>
	<?php endif; ?>
</div>
	