<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<head>
    <?php echo Core\Widgets::get('Head', $_seo); ?>
    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body class="indexPage">
    <?php foreach ( $_seo['scripts']['body'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <div class="wWrapper">
        <?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
        <div class="wContainer">
            <div class="<?php echo $_content_class ?>">
                <div class="wSize">
                    <div class="wMiddle">
                        <div class="errorContent">
                            <div class="errorHeader">404</div>
                            <div class="wTxt">
                                <h3>Страница не найдена</h3>
                                <p><em>К сожалению, страница, которую Вы запросили, не была найдена.</em> <br> Вы можете перейти на <a href="index.html">главную страницу</a> или ознакомиться с нашим <a href="#">каталогом товаров</a>.</p>
                                <p>В любом случае, у нас всегда есть, <a href="#">что вам предложить</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo Core\Widgets::get('Index_Advantages'); ?>
            <?php if ($_seo['h1']): ?>
                <div class="wSeoBlock">
                    <div id="cloneSeo"></div>
                </div>
            <?php endif ?>
        </div>
        <!-- .wConteiner -->
    </div>
    <?php echo Core\Widgets::get('Footer', array('counters' => Core\Arr::get($_seo, 'counters'), 'config' => $_config)); ?>
    <?php echo Core\Widgets::get('HiddenData'); ?>
    <!-- Внешние css файлы-->
    <link rel="stylesheet" property="stylesheet" href="<?php echo Core\HTML::link('/media/css/error.css', true); ?>">
</body>
</html>