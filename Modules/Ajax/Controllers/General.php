<?php
    namespace Modules\Ajax\Controllers;

    use Core\Arr;
    use Core\Common;
    use Core\Message;
    use Modules\Cart\Models\Cart;
    use Core\HTML;
	use Core\Widgets;

    class General extends \Modules\Ajax {

        public function removeConnectionAction() {
            $id = Arr::get($_POST, 'id');
            $row = Common::factory('users_networks')->getRow($id);
            if($row) {
                Common::factory('users_networks')->delete($id);
            }
            Message::GetMessage(1, 'Вы успешно удалили связь Вашего аккаунта и соц. сети!', 5000);
            $this->success();
        }
		
		// Add item to cart
		public function wbasketAddAction()
		{
			// Get and check incoming data
			$itemId = Arr::get($this->post, 'item_id', 0);

			if (!$itemId) {
				$this->error('No such item!');
			}

			$cart = Cart::factory();
			$cart->add($itemId);

			// Response
			$list = $cart->getList();
			$totals = $cart->getTotals();
			$recommend = $cart->itemsRecommend();
			
			$this->success(array('list' => $list, 'totals' => $totals, 'recommend' => $recommend));
			
		}

		// Edit item
		public function wbasketEditAction()
		{
			// Get and check incoming data
			$key = Arr::get($this->post, 'key');
			if (!$key && Arr::get($this->post, 'action') !== 'default') {
				$this->error('No such item!');
			}
			$count = Arr::get($this->post, 'count', 1);

			if (!in_array(Arr::get($this->post, 'action'), array('edit', 'delete', 'default'))) {
				$this->error();
			}

			$cart = new Cart;
			$recommend = $cart->itemsRecommend();
			if (Arr::get($this->post, 'action') == 'edit') {
				$cart->edit($key, $count);

				// Response
				$list = $cart->getList();
				$totals = $cart->getTotals();
				$this->success(array('list' => $list, 'totals' => $totals, 'recommend' => $recommend));
				
			} elseif (Arr::get($this->post, 'action') == 'delete') {
				$delete = (boolean)$cart->delete($key);

				// Response
				$totals = $cart->getTotals();
				$recommend = $cart->itemsRecommend();
				$this->answer(array('success' => $delete, 'totals' => $totals, 'recommend' => $recommend));
			}

			// Response
			$list = $cart->getList($key);
			$totals = $cart->getTotals();
			
			
			$this->success(array('list' => $list, 'totals' => $totals, 'recommend' => $recommend));
		}


        // Add item to cart
        /* public function addToCartAction() {
            // Get and check incoming data
            $catalog_id = Arr::get($this->post, 'id', 0);
            if( !$catalog_id ) {
                $this->error('No such item!');
            }
            // Add one item to cart
            Cart::factory()->add($catalog_id);
            $result = Cart::factory()->get_list_for_basket();
            $cart = array();
            foreach( $result AS $item ) {
                $obj = Arr::get($item, 'obj');
                if( $obj ) {
                    $cart[] = array(
                        'id' => $obj->id,
                        'name' => $obj->name,
                        'cost' => $obj->cost,
                        'image' => is_file(HOST.HTML::media('images/catalog/medium/'.$obj->image)) ? HTML::media('images/catalog/medium/'.$obj->image) : '',
                        'alias' => $obj->alias,
                        'count' => Arr::get($item, 'count', 1),
                    );
                }
            }
            $this->success(array('cart' => $cart));
        }


        // Edit count items in the cart
        public function editCartCountItemsAction() {
            // Get and check incoming data
            $catalog_id = Arr::get($this->post, 'id', 0);
            if( !$catalog_id ) {
                $this->error('No such item!');
            }
            $count = Arr::get($this->post, 'count', 0);
            if( !$count ) {
                $this->error('Can\'t change to zero!');
            }
            // Edit count items in cirrent position
            Cart::factory()->edit($catalog_id, $count);
            $this->success(array('count' => (int) Cart::factory()->_count_goods));
        }


        // Delete item from the cart
        public function deleteItemFromCartAction() {
            // Get and check incoming data
            $catalog_id = Arr::get($this->post, 'id', 0);
            if( !$catalog_id ) {
                $this->error('No such item!');
            }
            // Add one item to cart
            Cart::factory()->delete($catalog_id);
            $this->success(array('count' => (int) Cart::factory()->_count_goods));
        } */
		
		public function popupAction() {
            // Get and check incoming data
            $template = Arr::get($this->post, 'template');
            if( !$template ) {
                $this->error('No such template!');
            }
			$itemId = Arr::get($this->post, 'itemId');
            if($template == 'callorder' && !$itemId ) {
                $this->error('No such item!');
            }
            ob_clean();			
			echo Widgets::get('Popup_' . ucfirst($template), ['itemId' => $itemId]);
            exit;
        }

    }