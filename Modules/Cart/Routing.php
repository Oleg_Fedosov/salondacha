<?php
    
    return array(
        'cart' => 'cart/cart/index',
		'checkout' => 'cart/cart/checkout',
        'order' => 'cart/cart/order',
    );